#Assembled Hidden Toxin Genes
This repository contains data in associated with the publication: 

Macrander, J., Broe, M., Daly, M. (2015) Multi-copy venom genes hidden in de novo transcriptome assemblies, a cautionary tale with the snakelock sea anemone Anemonia sulcata (Pennant, 1977), *in prep as a Short Communication in Toxicon*