##Associated Publication Notes

8-26-2015 - Jason Macrander

The transcriptome used for this study is currently in prep for another publication and was not included here. The data associated with this analysis are included in the data directory, including the raw reads that were mapped to each toxin-like sequence and Geneious output files.

In addition to our approach above, we also merged the raw reads to a minimum length of 150 bases using PEAR (Zhang et al. 2014) and after and normalized using the fastx_collapser script from the FASTX-Toolkit (http://hannonlab.cshl.edu/fastx_toolkit/) attempted a de novo assembly using the program VTBuilder (Archer et al. 2014); however, the program failed to move beyond the Partitioning step.